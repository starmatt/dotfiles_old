include keymappings.conf

# Colorscheme
background #1a1d23
foreground #d3d7de

selection_background #4e576a
selection_foreground #d3d7de

# black
color0 #111317
color8 #4e576a

# red
color1 #f53d99
color9 #f53d99

# green
color2  #1ff98c
color10 #1ff98c

# yellow
color3  #ffc34d
color11 #ffc34d

# blue
color4  #558bf6
color12 #558bf6

# magenta
color5  #9580ff
color13 #9580ff

# cyan
color6  #8fafef
color14 #8fafef

# white
color7  #d3d7de
color15 #d3d7de

# others
color213 #c871f4
color232 #1a1d23
color233 #23272f

# Font
font_family      Input Nerd Font
bold_font        auto
italic_font      auto
bold_italic_font auto

font_size 10
font_features none

# Cursor
cursor #4d5eb3
cursor_text_color #1ff98c
cursor_shape block

# Scrollback
scrollback_lines 10000

# Mouse
url_color #8fafef
url_style curly

# Terminal bell
enable_audio_bell no

# Windows
remember_window_size  yes
initial_window_width  640
initial_window_height 400

enabled_layouts tall,fat,grid,stack

window_resize_step_lines 1
window_border_width 2
draw_minimal_borders no
window_margin_width 0
single_window_margin_width -1
window_padding_width 0
placement_strategy center
active_border_color #4e576a
inactive_border_color #23272f
bell_border_color #ffc34d
inactive_text_alpha 1.0
resize_draw_strategy static
resize_in_steps no
confirm_os_window_close 0

# Tab bar
tab_bar_edge bottom
tab_bar_style powerline
tab_bar_min_tabs 1
tab_switch_strategy previous
tab_activity_symbol none

tab_title_template "[{layout_name[:1].upper()}:{num_windows}] {title}"

active_tab_title_template "{fmt.fg._8fafef}[{fmt.fg._d3d7de}{layout_name[:1].upper()}{fmt.fg._558bf6}:{fmt.fg._1ff98c}{num_windows}{fmt.fg._8fafef}]{fmt.fg._d3d7de} {title}"

active_tab_foreground   #d3d7de
active_tab_background   #4e576a
active_tab_font_style   normal

inactive_tab_foreground #4e576a
inactive_tab_background #23272f
inactive_tab_font_style normal

tab_bar_background      #111317

# Advanced
update_check_interval 0
