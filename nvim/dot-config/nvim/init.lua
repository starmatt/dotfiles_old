-- ✨ Neovim configuration file
-- ----------------------------------------------------------------------------
-- Author:      Matthieu Borde <dev at starmatt dot net>
-- Website:     https://gitlab.com/starmatt

-- General settings
require('starmatt.settings')

-- General keymappings
require('starmatt.mappings')

-- Plugins
require('starmatt.plugins')
require('starmatt.lsp')

-- Colorscheme
-- require('starmatt')
