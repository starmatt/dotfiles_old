-- Wrapper around nvim_replace_termcodes
local function tc(str)
    return vim.api.nvim_replace_termcodes(str, true, true, true)
end

-- Set a key mapping
local function map(mode, lhs, rhs, opts)
    -- Default options
    local options = { noremap = true, silent = true }
    local buffer = nil

    if opts then
        buffer = opts.buffer or nil
        opts.buffer = nil
        options = vim.tbl_extend('force', options, opts)
    end

    if buffer then
        vim.api.nvim_buf_set_keymap(buffer, mode, lhs, rhs, options)
    else
        vim.api.nvim_set_keymap(mode, lhs, rhs, options)
    end
end

return {
    tc = tc,
    map = map
}
