local indent = 4
local width = 80

vim.cmd('syntax enable')
vim.cmd('filetype plugin indent on')
vim.cmd('au TextYankPost * lua vim.highlight.on_yank { on_visual = false }') -- Highlight on yank)

vim.g.mapleader = ' ' -- Map <Leader> to space
vim.g.maplocalleader = ' '

-- vim.opt.autoindent = true                 -- Enable auto-indenting
vim.opt.clipboard = 'unnamed,unnamedplus' -- Use system clipboard
-- vim.opt.colorcolumn = tostring(width)     -- Line length marker
vim.opt.completeopt = {                   -- Completion options
    'menuone',
    'noinsert',
    'noselect',
}
vim.opt.cursorline = true                 -- Highlight cursor line
vim.opt.expandtab = true                  -- Use spaces instead of tabs
vim.opt.fillchars = { vert = '█' }
vim.opt.formatoptions = 'crqnj'           -- Automatic formatting options
vim.opt.hidden = true                     -- Enable background buffers
vim.opt.ignorecase = true                 -- Ignore case
vim.opt.joinspaces = false                -- No double spaces with join
vim.opt.list = true                       -- Show some invisible characters
vim.opt.number = true                     -- Show line numbers
vim.opt.pastetoggle = '<F2>'              -- Paste mode
vim.opt.pumheight = 24                    -- Max height of popup menu
vim.opt.scrolloff = 4                     -- Lines of context
vim.opt.shiftround = true                 -- Round indent
vim.opt.shiftwidth = indent               -- Size of an indent
vim.opt.shortmess = 'atToOFc'             -- Prompt message options
vim.opt.sidescrolloff = 8                 -- Columns of context
-- vim.opt.signcolumn = 'yes'                -- Show sign column
vim.opt.smartcase = true                  -- Do not ignore case with capitals
vim.opt.smartindent = true                -- Insert indents automatically
vim.opt.splitbelow = true                 -- Put new windows below current
vim.opt.splitright = true                 -- Put new windows right of current
vim.opt.tabstop = indent                  -- Number of spaces tabs count for
vim.opt.termguicolors = true              -- True color support
vim.opt.textwidth = width                 -- Maximum width of text
vim.opt.timeoutlen = 500
vim.opt.updatetime = 100                  -- Delay before swap file is saved
vim.opt.sessionoptions:append({           -- store tabpages and globals in session
    'tabpages',
    'globals',
})
-- vim.opt.wildmode = {'list', 'longest'}    -- Command-line completion mode
