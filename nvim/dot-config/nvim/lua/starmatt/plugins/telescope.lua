local ok, telescope = pcall(require, 'telescope')

if not ok then
    vim.notify([[ 'telescope' not found! ]])
    return
end

telescope.setup({})
