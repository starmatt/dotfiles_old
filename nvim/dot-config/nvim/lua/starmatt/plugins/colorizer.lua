local ok, colorizer = pcall(require, 'colorizer')

if not ok then
    vim.notify('Colorizer not found!')
    return
end

colorizer.setup({ '*' }, {
    names = false,
    css = true,             -- Enable all CSS features: rgb_fn, hsl_fn, names, RGB, RRGGBB
    css_fn = true,          -- Enable all CSS *functions*: rgb_fn, hsl_fn
    -- Available modes: foreground, background
    mode = 'background',    -- Set the display mode.
});
