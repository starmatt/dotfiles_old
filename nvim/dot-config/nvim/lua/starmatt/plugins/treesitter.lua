local ok, ts = pcall(require, 'nvim-treesitter.configs')

if not ok then
    vim.notify([[ 'nvim-treesitter' not found! ]])
    return
end

ts.setup({
    ensure_installed = "all",
    ignore_install = {}, -- List of parsers to ignore installing

    autopairs = {
        enable = true,
    },

    highlight = {
        enable = true, -- false will disable the whole extension
        disable = {}, -- list of language that will be disabled
        -- additional_vim_regex_highlighting = true,
    },

    indent = {
        enable = true
    },

    context_commentstring = {
        enable = true,
        enable_autocmd = false,
    },
})
