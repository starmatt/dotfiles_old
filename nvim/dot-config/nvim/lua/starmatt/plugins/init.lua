-- Auto-install packer.nvim
local packer_url = 'https://github.com/wbthomason/packer.nvim'

local install_path = table.concat({
    vim.fn.stdpath('data'),
    '/site/pack/packer/start/packer.nvim'
})

if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
    vim.fn.system({ 'git', 'clone', packer_url, install_path })
    vim.cmd('packadd packer.nvim')
end

-- Autocommand that reloads neovim whenever the file is saved
vim.cmd([[
    augroup packer_user_config
        autocmd!
        autocmd BufWritePost */starmatt/plugins/init.lua source % | PackerSync
    augroup end
]])

local ok, packer = pcall(require, 'packer')

if not ok then
    vim.notify([[ 'packer' not found! ]])
    return
end

-- Packer options
packer.init({
    display = {
        open_fn = function()
            return require('packer.util').float({
                border = 'rounded'
            })
        end,
    },
})

-- Use plugins!
packer.startup(function (use)
    -- Packer (manages itself)
    use({ packer_url })

    -- Many plugins depend on these
    use({ 'https://github.com/nvim-lua/popup.nvim' })
    use({ 'https://github.com/nvim-lua/plenary.nvim' })
    use({ 'https://github.com/kyazdani42/nvim-web-devicons' })

    -- LSP
    use({ 'https://github.com/neovim/nvim-lspconfig' })
    use({ 'https://github.com/williamboman/nvim-lsp-installer' })

    -- Null-ls (formatting and diagnostics with LSP)
    use({ 'https://github.com/jose-elias-alvarez/null-ls.nvim' })

    -- Treesitter (über highlights)
    use({
        'https://github.com/nvim-treesitter/nvim-treesitter',
        run = ':TSUpdate'
    })

    -- Playground (play with treesitter!)
    use({ 'https://github.com/nvim-treesitter/playground' })

    -- Treesitter-aware commentstring
    use({ 'https://github.com/JoosepAlviste/nvim-ts-context-commentstring' })

    -- Completion (u complete me)
    use({ 'https://github.com/hrsh7th/nvim-cmp' }) -- The completion plugin
    use({ 'https://github.com/hrsh7th/cmp-buffer' }) -- buffer completions
    use({ 'https://github.com/hrsh7th/cmp-path' }) -- path completions
    use({ 'https://github.com/hrsh7th/cmp-cmdline' }) -- cmdline completions
    use({ 'https://github.com/saadparwaiz1/cmp_luasnip' }) -- snippet completions
    use({ 'https://github.com/hrsh7th/cmp-nvim-lsp' }) -- lsp completions
    use({ 'https://github.com/hrsh7th/cmp-nvim-lua' }) -- vim completions

    -- Snippets
    use({ 'https://github.com/L3MON4D3/LuaSnip' }) -- snippet engine
    use({ 'https://github.com/rafamadriz/friendly-snippets' }) -- a bunch of snippets to use

    -- Telescope (fuzzy file finder, live grep, and more!)
    use({ 'https://github.com/nvim-telescope/telescope.nvim' })

    use({ 'https://github.com/startup-nvim/startup.nvim' })

    -- Comment (text in and out)
    use({ 'https://github.com/numToStr/Comment.nvim' })

    -- Colorizer (color highlighter)
    use({ 'https://github.com/norcalli/nvim-colorizer.lua' })

    -- Which key (display keybindings)
    use({ 'https://github.com/folke/which-key.nvim' })

    -- Autopairs, integrates with both cmp and treesitter
    use({ 'https://github.com/windwp/nvim-autopairs' })

    -- Git (signs)
    use({ 'https://github.com/lewis6991/gitsigns.nvim' })

    -- Nvim-tree (file explorer)
    use({ 'https://github.com/kyazdani42/nvim-tree.lua' })

    -- Lines (LINES)
    use({ 'https://github.com/nvim-lualine/lualine.nvim' })

    -- Colorscheme
    use({ '/home/matt/projects/starmatt-colors.nvim' })
    use({ 'https://github.com/catppuccin/nvim', as = 'catppuccin' })
end)

-- Plugins configuration
require('nvim-web-devicons').setup()
require('starmatt.plugins.nvim-cmp')
require('starmatt.plugins.treesitter')
require('starmatt.plugins.telescope')
require('starmatt.plugins.colorizer')
require('starmatt.plugins.which-key')
require('starmatt.plugins.autopairs')
require('starmatt.plugins.comment')
require('starmatt.plugins.gitsigns')
require('starmatt.plugins.nvim-tree')
require('starmatt.plugins.lualine')
require('starmatt.plugins.startup')

vim.cmd [[colorscheme catppuccin]]
-- require('starmatt')
