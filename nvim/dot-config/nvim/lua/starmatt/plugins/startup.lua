local ok, startup = pcall(require, 'startup')

if not ok then
    vim.notify([[ 'startup' not found! ]])
    return
end

startup.setup()
