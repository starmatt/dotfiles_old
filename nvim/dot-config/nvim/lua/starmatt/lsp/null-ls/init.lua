local ok, null_ls = pcall(require, 'null-ls')

if not ok then
    vim.notify([[ 'null-ls' not found! ]])
    return
end

null_ls.setup({
    debug = false,

    sources = {
        null_ls.builtins.formatting.prettier,
    },
})
