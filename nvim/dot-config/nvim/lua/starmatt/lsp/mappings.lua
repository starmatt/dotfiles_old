local map = function(lhs, rhs, opts)
    local utils = require('starmatt.utils')
    utils.map('n', lhs, '<cmd>lua ' .. rhs .. '<CR>', opts)
end

local general = function()
    map('<leader>q', 'vim.diagnostic.setloclist()')
    map('ge', 'vim.diagnostic.open_float(0, { float = { border = "rounded" }})')
    map('gp', 'vim.diagnostic.goto_prev({ float = { border = "rounded" }})')
    map('gn', 'vim.diagnostic.goto_next({ float = { border = "rounded" }})')
end

local on_attach = function (buffer)
    local opts = { buffer = buffer }

    map('<C-k>', 'vim.lsp.buf.signature_help()', opts)
    map('<leader>D', 'vim.lsp.buf.type_definition()', opts)
    map('<leader>ca', 'vim.lsp.buf.code_action()', opts)
    map('<leader>f', 'vim.lsp.buf.formatting()', opts)
    map('<leader>rn', 'vim.lsp.buf.rename()', opts)
    map('<leader>wa', 'vim.lsp.buf.add_workspace_folder()', opts)
    map('<leader>wl', 'print(vim.inspect(vim.lsp.buf.list_workspace_folders()))', opts)
    map('<leader>wr', 'vim.lsp.buf.remove_workspace_folder()', opts)
    map('K', 'vim.lsp.buf.hover()', opts)
    map('gD', 'vim.lsp.buf.declaration()', opts)
    map('gd', 'vim.lsp.buf.definition()', opts)
    map('gi', 'vim.lsp.buf.implementation()', opts)
    map('gr', 'vim.lsp.buf.references()', opts)
end

return {
    general = general,
    on_attach = on_attach,
}
