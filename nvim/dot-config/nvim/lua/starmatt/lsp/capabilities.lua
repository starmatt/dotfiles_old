local ok, cmp_nvim_lsp = pcall(require, 'cmp_nvim_lsp')

if not ok then
    vim.notify([[ 'cmp_nvim_lsp' not found! ]])
    return
end

local capabilities = vim.lsp.protocol.make_client_capabilities()

return cmp_nvim_lsp.update_capabilities(capabilities)
