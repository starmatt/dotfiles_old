local lsp_highlight_document = function(client)
    -- Set autocommands conditional on server_capabilities
    if client.resolved_capabilities.document_highlight then
        vim.api.nvim_exec([[
            augroup lsp_document_highlight
                autocmd! * <buffer>
                autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
                autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
            augroup END
        ]], false)
    end
end

local on_attach = function(client, buffer)
    if client.name == 'tsserver' then
        client.resolved_capabilities.document_formatting = false
    end

    require('starmatt.lsp.mappings').on_attach(buffer)
    vim.cmd([[ command! Format execute 'lua vim.lsp.buf.formatting()' ]])
    lsp_highlight_document(client)
end

return on_attach;
