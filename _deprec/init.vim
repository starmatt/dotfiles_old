"""""""""""
" Plugins "
"""""""""""

let g:polyglot_disabled = [] " Disable specific languages in Polyglot (needs to be set before loading the plugin)

call plug#begin(stdpath('data') . '/plugged')

" Plug 'https://github.com/vim-airline/vim-airline'
Plug 'https://github.com/machakann/vim-highlightedyank'
Plug 'https://github.com/machakann/vim-sandwich'
Plug 'https://github.com/tpope/vim-commentary'
Plug 'https://github.com/suy/vim-context-commentstring'
" Plug 'https://github.com/sheerun/vim-polyglot'
Plug 'https://github.com/nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'https://github.com/nvim-treesitter/playground'
" Plug 'https://github.com/ap/vim-css-color'
Plug 'https://github.com/bronson/vim-trailing-whitespace'
Plug 'https://github.com/suan/vim-instant-markdown', {'for': 'markdown'}
Plug 'https://github.com/mattn/emmet-vim'
Plug 'https://github.com/tpope/vim-fugitive'
Plug 'https://github.com/terryma/vim-multiple-cursors'
Plug 'https://github.com/w0rp/ale'
Plug 'https://github.com/benknoble/vim-auto-origami'
Plug 'https://github.com/hail2u/vim-css3-syntax'
" Plug 'https://github.com/leafOfTree/vim-vue-plugin'
Plug 'https://github.com/junegunn/fzf', {'do': { -> fzf#install() }}
Plug 'https://github.com/junegunn/fzf.vim'
Plug '~/code/starmatt-colors-ts'
" Plug 'https://gitlab.com/starmatt/starmatt-colors'

call plug#end()

"""""""""""
" General "
"""""""""""

set nu " Set line numbers
set mouse=a " Enable mouse
set cursorline " Enable cursorline
set tabstop=4 shiftwidth=4 expandtab " Set tabs as 4 spaces
set fillchars+=vert:█
set termguicolors " Enable GUI colors to use truecolors in vim
set showtabline=2
set noshowmode
set wildmenu
set lazyredraw
set iskeyword+=-
let g:mapleader = "\<Space>"
let g:vim_indent_cont = &sw

" Switch Up, Down and Left, Right in wildmenu
inoremap <expr> <Esc> pumvisible() ? "\<C-e>" : "\<Esc>"
inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<CR>"
inoremap <expr> <Down> pumvisible() ? "\<C-n>" : "\<Down>"
inoremap <expr> <Up> pumvisible() ? "\<C-p>" : "\<Up>"

" Copy to clipboard
vnoremap <leader>y "+y
nnoremap <leader>Y "+yg_
nnoremap <leader>y "+y
nnoremap <leader>yy "+yy

" Paste from clipboard
nnoremap <leader>p "+p
nnoremap <leader>P "+P
vnoremap <leader>p "+p
vnoremap <leader>P "+P

" colorscheme
syntax on
colorscheme starmatt

" displays highlight syntax group under cursor
function! SynGroup()
    let l:s = synID(line('.'), col('.'), 1)
    echo synIDattr(l:s, 'name') . ' -> ' . synIDattr(synIDtrans(l:s), 'name')
endfun

"""""""""""""""""""""""""
" Plugins configuration "
"""""""""""""""""""""""""

" NERD Commenter: https://github.com/preservim/nerdcommenter#usage

let g:NERDSpaceDelims = 1
let g:NERDCompactSexyComs = 1
let g:NERDTrimTrailingWhitespace = 1
let g:NERDToggleCheckAllLines = 1

" Airline: https://github.com/vim-airline/vim-airline#documentation

" let g:airline_theme='starmatt'
" let g:airline_powerline_fonts = 1 " Enables Powerline fonts
" let g:airline_skip_empty_sections = 1
" let g:airline#extensions#tabline#enabled = 1 " Adds a bar on top with tabs information

" FZF: https://github.com/junegunn/fzf.vim#customization

autocmd! FileType fzf set laststatus=0 noshowmode noruler
    \| autocmd BufLeave <buffer> set laststatus=2 showmode ruler " Hide ugly statusline

nnoremap <leader><space> :Files<CR>
nnoremap <leader>a :Buffers<CR>
nnoremap <leader>A :Windows<CR>
nnoremap <leader>; :BLines<CR>
nnoremap <leader>o :BTags<CR>
nnoremap <leader>O :Tags<CR>
nnoremap <leader>? :History<CR>

let g:fzf_action = {
    \ 'ctrl-e':     'edit',
    \ 'ctrl-t':     'tab split',
    \ 'ctrl-s':     'split',
    \ 'ctrl-v':     'vsplit'
    \ } " Custom keybindings

let g:fzf_history_dir = '~/.local/share/fzf-history' " Enable per-command history (CTRL+P, CTRL+N)

" Instant Markdown: https://github.com/instant-markdown/vim-instant-markdown#configuration

let g:instant_markdown_port = 8888
let g:instant_markdown_logfile = '/tmp/instant_markdown.log'

" ALE: https://github.com/dense-analysis/ale#usage

nmap <F6> <Plug>(ale_fix)

let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] [%severity%] %s (%code%)'

let g:ale_fixers = {
    \ 'javascriptreact': ['eslint'],
    \ 'javascript': ['prettier', 'eslint'],
    \ 'vue': ['prettier',  'eslint']
    \ }

let g:ale_fix_on_save = 1

" Auto Origami: https://github.com/benknoble/vim-auto-origami/blob/master/README.md

augroup autofoldcolumn
    au!
    au CursorHold,BufWinEnter,WinEnter * AutoOrigamiFoldColumn
augroup END

" Vim Vue: https://github.com/leafOfTree/vim-vue-plugin#configuration

let g:vim_vue_plugin_config = {
    \ 'full_syntax': ['html', 'javascript', 'css'],
    \ 'initial_indent': [],
    \ 'attribute': 0,
    \ 'keyword': 0,
    \ 'foldexpr': 0,
    \ 'debug': 0,
    \ }

function! OnChangeVueSyntax(syntax)
    echo 'Syntax is '.a:syntax
    if a:syntax == 'html'
        setlocal commentstring=<!--%s-->
        setlocal comments=s:<!--,m:\ \ \ \ ,e:-->
    elseif a:syntax =~ 'css'
        setlocal comments=s1:/*,mb:*,ex:*/ commentstring&
    else
        setlocal commentstring=//%s
        setlocal comments=sO:*\ -,mO:*\ \ ,exO:*/,s1:/*,mb:*,ex:*/,://
    endif
endfunction

lua <<EOF
require'nvim-treesitter.configs'.setup {
  ensure_installed = "all", -- one of "all", "maintained" (parsers with maintainers), or a list of languages
  highlight = {
    enable = true,              -- false will disable the whole extension
    disable = { "c", "rust" },  -- list of language that will be disabled
  },
}
EOF
