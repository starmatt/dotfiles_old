local wezterm = require('wezterm')

return {
    exit_behavior = 'Close',
    font = wezterm.font_with_fallback({
        { 'Input Nerd Font' },
        { 'JetBrains Mono' },
    }),
    font_size = 10,
    hide_tab_bar_if_only_one_tab = true,
    tab_bar_at_bottom = true,
    use_fancy_tab_bar = false,
    dpi = 144.0,

    inactive_pane_hsb = {
        saturation = 1.0,
        brightness = 0.5,
    }
}
