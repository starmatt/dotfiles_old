local config = {}

local parts = {
    mappings = require('starmatt.mappings'),
    ui = require('starmatt.ui'),
}

for _, part in pairs(parts) do
    for key, value in pairs(part) do
        config[key] = value
    end
end

return config;
